
extern crate crossbeam_channel;

use std::sync::Arc;
use crossbeam_channel::{unbounded, Sender, Receiver, TryRecvError};
use std::cell::UnsafeCell;
use std::mem::{self, ManuallyDrop };

#[derive(Clone)]
struct FreeList<A, F> 
where F: Fn () -> A {
    rx: Receiver<A>,
    tx: Sender<A>,
    alloc: F,
}

struct Handle<A, F>
where F: Fn () -> A {
    tx: Sender<A>,
    alloc: F,
    payload: ManuallyDrop<A>,
}

impl Drop for Handle {

    // Take our payload (allocated thingy) and put it back into the freelist
    fn drop(&mut self) {

        // we're about to destroy self, so nothing will reference this uninitialized mem
        // the ManuallyDrop wrapper should make sure of that
        let p = mem::replace(&mut self.payload, ManuallyDrop::new(unsafe { mem::uninitialized() }));

        // we don't actually want to know the result of this try_send
        // it can't fail due to lack of capacity, and if it fails due
        // to Disconnected, that means that the parent FreeList has been dropped
        // and there will be nothing to help this
        let _ = self.tx.try_send(p.into_inner()); 
    }
}

impl<'handle, T: ?Sized> Deref for MutexGuard<'handle, T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.payload.deref()
    }
}

impl<'handle, T: ?Sized> DerefMut for MutexGuard<'handle, T> {
    fn deref_mut(&mut self) -> &mut T {
        self.payload.deref_mut()
    }
}

impl<A, F> FreeList<A, F> 
  where F: Fn() -> A {

    pub fn new(f: F) -> FreeList<A, F> {
        let (tx, rx) = unbounded();
        FreeList {
            rx,
            tx,
            alloc: F,
        }
    }

    pub fn get(&self) -> Handle<A> {
        let obj = 
            match self.rx.try_recv() {
                Ok(a) => a 
                Err(TryRecvError::Empty) => {
                    (*self.alloc)();
                },
                Err(TryRecvError::Disconnected) => {
                    panic!("FreeList Queue is in a bad state. This shouldn't happen")
                }
            };
        Handle {
            tx: self.tx.clone(),
            alloc: self.alloc,
            payload: Some(obj),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
